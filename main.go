package main

import (
	"github.com/arsmn/fiber-swagger"
	"github.com/gofiber/fiber"
	"github.com/gofiber/helmet"
	_ "record-rating/docs"
	"record-rating/internal/rate"
)

// @title Simple Record Rating
// @version 1.0
// @description Just a simple record rating service
// @termsOfService https://themicroservice.co/t-and-c/
// @contact.name API Support
// @contact.email api@themicroservice.co
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host https://api.themicroservice.co/rating/
// @BasePath /
func main() {
	app := fiber.New()
	app.Use(helmet.New())

	app.Use("/swagger", swagger.Handler)

	app.Post("/", RateRecord)

	if err := app.Listen(3000); err != nil {
		panic(err)
	}
}

// RateRecord godoc
// @Summary Record rating
// @Description Rates a unit based record given a configuration and the units consumed.
// @Tags kill-billing
// @Accept  json
// @Produce  json
// @Param rating body rate.Rating true "Rate record"
// @Success 200 {object} rate.RatingResult
// @Router /rating [post]
func RateRecord(ctx *fiber.Ctx) {
	request := &rate.Rating{}

	if err := ctx.BodyParser(request); err != nil {
		ctx.Status(fiber.StatusBadRequest).Send(err)
	}

	ratingResult := rate.CalculateRating(request)

	_ = ctx.JSON(ratingResult)
	return
}