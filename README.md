# Kill Billing : Record Rating 

Record rating is a high performance, but dead simple record rating microservice for unit based billing requirements.

Taking a number of units, and a rating configuration, we'll calculate the final price and units charged.

Input:


``` json
{
    "units": 5,
    "unit_label": "megabyte",
    "unit_label_plural": "megabytes",
    "rate": {
        
        "initial_price": 0.20,

        "minimum_price": 1.0000000000,
        "maximum_price": 50.0000000000,

        "minimum_chargeable_units": 10,
        "maximum_chargeable_units": 200,
        
        "steps": [
            {
                "units_per_block": 2,
                "block_price": 0.1000000000,
                "blocks_in_step": 2
            },
            {
                "units_per_block": 1,
                "block_price": 0.5000000000,
                "blocks_in_step": null
            }
        ]
    }
}

```

## Explanation of the "rate" element

Initial price - This is always added to the "final_price" for the record. Basically, it's the starting amount

Minimum price - This is the minimum amount the final_price can be - this includes the initial_price.

Maximum price - This is the maximum amount the final_price can be. 

Minimum chargeable units - This is the minimum the "units" can be for a record. If we get units = 5, but minimum units = 10, then units becomes 10.

Maximum chargeable units - Like minimum chargeable but in reverse.

Steps:

  Units per block - this is the number of units to subtract from the "units" in each iteration of the charge block. So if the units was 100, and the units in block was 10, then the first charge for the block would make the remaining units to charge = 90.
  
  The block price is the cost of the units in block.
  
  Blocks in step are the number of times to charge at this block price/units
  
  If blocks in step is null, you keep charging at that block.
  
  
  ## Output
  
  
  ``` json
  {
      "units_charged": 0,
      "final_price": 0.0000000000
  }
  
  ```
  