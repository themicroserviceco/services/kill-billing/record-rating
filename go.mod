module record-rating

go 1.15

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/arsmn/fiber-swagger v1.3.3
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/go-openapi/spec v0.19.9 // indirect
	github.com/go-openapi/swag v0.19.9 // indirect
	github.com/gofiber/fiber v1.14.5
	github.com/gofiber/helmet v0.1.2
	github.com/mailru/easyjson v0.7.6 // indirect
	github.com/swaggo/swag v1.6.7
	github.com/urfave/cli/v2 v2.2.0 // indirect
	golang.org/x/net v0.0.0-20200904194848-62affa334b73 // indirect
	golang.org/x/tools v0.0.0-20200904185747-39188db58858 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
