package rate

// Rating ...
type Rating struct {
	Units           int    `json:"units,omitempty"`
	UnitLable       string `json:"unit_label,omitempty"`
	UnitLabelPlural string `json:"unit_label_plural,omitempty"`
	Rate            *Rate  `json:"rate,omitempty"`
}

// RatingResult ...
type RatingResult struct {
	UnitsCharged int     `json:"units_charged"`
	FinalPrice   float64 `json:"final_price"`
}

// Rate ...
type Rate struct {
	InitialPrice          float64 `json:"initial_price,omitempty"`
	MinimumPrice          float64 `json:"minimum_price,omitempty"`
	MaximumPrice          float64 `json:"maximum_price,omitempty"`
	MinimumChargableUnits int     `json:"minimum_chargeable_units,omitempty"`
	MaximumChargableUnits int     `json:"maximum_chargeable_units,omitempty"`
	Steps                 []*Step `json:"steps,omitempty"`
}

// Step ...
type Step struct {
	UnitsPerBlock int     `json:"units_per_block,omitempty"`
	BlockPrice    float64 `json:"block_price,omitempty"`
	BlocksInStep  int     `json:"blocks_in_step,omitempty"`
}
