package rate

// setRateUnitLimit Set the units according the limits configured in MinimumChargeableUnits and MaximumChargeableUnits
func setRateUnitLimit(rating *Rating) {

	if rating.Units < rating.Rate.MinimumChargableUnits {
		rating.Units = rating.Rate.MinimumChargableUnits

	} else if rating.Units > rating.Rate.MaximumChargableUnits {
		rating.Units = rating.Rate.MaximumChargableUnits
	}

	return
}

// setPriceLimit Sets the minimum and/or maximum amount to be charged in a rating process
func setPriceLimit(ratingRequest *Rating, ratingResult *RatingResult) {

	if ratingResult.FinalPrice < ratingRequest.Rate.MinimumPrice {
		ratingResult.FinalPrice = ratingRequest.Rate.MinimumPrice

	} else if ratingResult.FinalPrice > ratingRequest.Rate.MaximumPrice {
		ratingResult.FinalPrice = ratingRequest.Rate.MaximumPrice
	}

	return
}
