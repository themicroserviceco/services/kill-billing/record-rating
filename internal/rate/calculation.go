package rate

// CalculateRating calculates the rating value based on block units, block prices,
// block in steps, and also based on minimum and maximum amounts to be chargeable
func CalculateRating(rating *Rating) *RatingResult {

	var result = &RatingResult{}

	result.FinalPrice = rating.Rate.InitialPrice
	setRateUnitLimit(rating)

	for _, step := range rating.Rate.Steps {

		if result.UnitsCharged >= rating.Units {
			break
		}

		// This is the same as step.BlocksInStep == nil | null, since numeric types does not store nil
		// references the default value is always 0 or 0.0 for floating points
		if step.BlocksInStep == 0 {
			for {
				if result.UnitsCharged >= rating.Units {
					break
				}

				result.UnitsCharged += step.UnitsPerBlock
				result.FinalPrice += step.BlockPrice

			}

		} else {
			for i := step.BlocksInStep; i > 0; i-- {
				if result.UnitsCharged >= rating.Units {
					break
				}

				result.UnitsCharged += step.UnitsPerBlock
				result.FinalPrice += step.BlockPrice

			}
		}
	}

	setPriceLimit(rating, result)
	return result
}
